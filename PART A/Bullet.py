import pygame

class Bullet(pygame.sprite.Sprite):
    """Class to manage bullets fired from the ship"""

    def __init__(self, settings, screen, ship):
        """Create a bullet at the ship's position"""

        super(Bullet, self).__init__()

        self.image = pygame.image.load('images/bullet.png')

        self.screen = screen

        self.rect = pygame.Rect(ship.rect.centerx-32, ship.rect.top-64, settings.bullet_width, settings.bullet_height)


    def draw(self):
        """Draw the ship at its current location."""
        self.screen.blit(self.image, self.rect) # draws the Ship image into the screen


    def update(self):
        self.rect.centery -=1;
