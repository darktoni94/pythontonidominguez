var net = require('net');
var ClientsList = require('./ClientsList.js');

//////////////////
var PORT = 43234;
var ClientsList;

main();

function main() {

	ClientsList = new ClientsList();

	initServer(ClientsList);

	var consoleInput = function() {
		input( ">> ", function(data) {
			var parts = data.trim().split(' ');
			if (parts[0]) parts[0] = parts[0].toUpperCase();

			switch (true) {
				case (parts[0]=="Q"):
				//THIS COMMAND QUITS THE SERVER EXECUTION
					process.exit(0);
					break;
				case (parts[0]=="LST"):
				//THIS COMMAND LISTS THE CLIENTS
					var myclientList = ClientsList.getChannels();
					console.log(myclientList);
					break;
				case (parts[0]=="LST [CHANNEL]"):
				//THIS COMMAND LISTS THE USERS CONNECTED TO THE CHANNEL
					var mysub = ClientsList.getSubscribersDetail();
					console.log(mysub);
				case (parts[0]=="SEND"):
					//Hint ClientsList.getSocket(parts[1]);
					break;
				case (parts[0]=="SENDALL"):
					//Hint: use ClientsList.getSubscribersDetail() and ClientsList.send();
					break;
				case (parts[0]=="WHEREIS"):
					if (parts[1]) {
						var details = clientsList.getSubscriberDetail(parts[1]);
						console.log ( JSON.stringify(  details ) );
					}
					setTimeout( consoleInput, 300);
					break;
				default:
					console.log("Incorrect command");
					consoleInput();
					break;
			}

		});
	};

	console.log("----------------------------")
	console.log("LST - List regsitered CHANNELS and its sockets and stats");
	console.log("LST [CHANNEL] - List regsitered subscribers in the CHANNEL");
	console.log("WHEREIS SUBSCRIBER - List CHANNELS and sockets of subscriber SUBSCRIBER");
	console.log("SEND [SUBSCRIBER] [MESSAGE] - Send MESSAGE to the SUBSCRIBER client identified")
	console.log("SENDALL [CHANNEL] [MESSAGE] - Send MESSAGE to all subscribers in the CHANNEL")
	console.log("----------------------------")
	console.log("Q - Exit");
	console.log("----------------------------")

	consoleInput();
}


function initServer(clientsList) {
	net.createServer(function(sock) {
		sock.id = Math.floor(Math.random() * 100000);
			console.log('CONNECTION RECEIVED: ' + sock.remoteAddress +':'+ sock.remotePort);
	    sock.on('data', function(data) {
	    	var _self = this;
	    	console.log('DATA ' + sock.remoteAddress + ': ' + data);
	    	data.toString('utf-8').split('\n').forEach(function(message){
	    		parseData(clientsList, message, _self);
	    	});
	    });
	    sock.on('close', function(data) {
	    	console.log('Connection closed for socket ' + sock.id);
	    	clientsList.removeSocket(sock.id);
	    });
	    sock.on('error', function (err) {
		    console.log("Socket error " + sock.id, err.stack);
		    clientsList.removeSocket(sock.id);
		});
	    clientsList.addClient(null, sock, sock.id);
	}).listen(PORT);
	console.log('Server listening on port '+ PORT);
}

function parseData(clientsList, message, clientSocked) {
	console.log("Parsing message: " + message + "/n");
	// To register users:
	// clientsList.setChannelToClient(sock.id, id_player, id_channel);
	// Example: var parts = message.trim().split(':');
	// var game = part[1];
	// var player = parts[2];
	// clientsList.setChannelToClient(sock.id, player, game);
}


function input(question, callback) {
	 var stdin = process.stdin, stdout = process.stdout;

	 stdin.resume();
	 stdout.write(question);

	 stdin.once('data', function(data) {
	   data = data.toString().trim();
	   callback(data);
	 });
}
